/*global jQuery, document*/
jQuery(document).ready(function ($) {
    $('#popnavbarmenutoggle').click(function(e){
        e.preventDefault();
        $('#popnavbarmenu').slideToggle();
        $(this).toggleClass('open');
    });
});