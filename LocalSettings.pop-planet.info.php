<?php
# This file was automatically generated by the MediaWiki 1.31.0
# installer. If you make manual changes, please keep track in case you
# need to recreate them later.
#
# See includes/DefaultSettings.php for all configurable settings
# and their default values, but don't forget to make changes in _this_
# file, not there.
#
# Further documentation for configuration settings may be found at:
# https://www.mediawiki.org/wiki/Manual:Configuration_settings

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

##
## General settings
##

$wgSitename = "Pop!_Planet";
$wgMetaNamespace = "Pop!_Planet";

## The protocol and server name to use in fully-qualified URLs
$wgServer = "https://wiki.pop-planet.info";

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## The URL path to the logo.  Make sure you change this from the default,
## or else you'll overwrite your logo when you upgrade!
## (Commented out, it's not shown anywhere)
#$wgLogo = "$wgResourceBasePath/resources/assets/wiki.png";

# Periodically send a pingback to https://www.mediawiki.org/ with basic data
# about this MediaWiki instance. The Wikimedia Foundation shares this data
# with MediaWiki developers to help guide future development efforts.
$wgPingback = true;

# Site language code, should be one of the list in ./languages/data/Names.php
$wgLanguageCode = "en";

$wgSecretKey = "";

# Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";

# Site upgrade key. Must be set to a string (default provided) to turn on the
# web installer while LocalSettings.php is in place
$wgUpgradeKey = "gpsxum5pcc5hnw1y";

## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgRightsPage = ""; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "http://www.gnu.org/copyleft/fdl.html";
$wgRightsText = "GNU Free Documentation License 1.3 or later";
$wgRightsIcon = "$wgScriptPath/resources/assets/licenses/gnu-fdl.png";

$wgJobRunRate = 0;
$wgRunJobsAsync = 1;

# Enable subpages in the main namespace (FS#39668)
$wgNamespacesWithSubpages[NS_MAIN] = true;

# Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";


##
## Email settings
##

## UPO means: this is also a user preference option

$wgEnableEmail = true;
$wgEnableUserEmail = true; # UPO

$wgEmergencyContact = "dgriffiths@pop-planet.info";
$wgPasswordSender = "info@pop-planet.info";
$wgNoReplyAddress = $wgPasswordSender;

$wgEnotifUserTalk = true; # UPO
$wgEnotifWatchlist = true; # UPO
$wgEmailAuthentication = true;

# avoid bouncing of user-to-user emails (FS#26737)
$wgUserEmailUseReplyTo = true;


##
## Cache and performance settings
##

## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;

## Shared memory settings
$wgMainCacheType = CACHE_NONE;
$wgMemCachedServers = [];

## Set $wgCacheDirectory to a writable directory on the web server
## to make your wiki go slightly faster. The directory should not
## be publically accessible from the web.
$wgTmpDirectory = "/tmp/wiki";
$wgCacheDirectory = "$wgTmpDirectory/cache/data";
$wgShowIPinHeader = false;
$wgEnableSidebarCache = true;
$wgUseFileCache = true;
$wgFileCacheDirectory = "$wgTmpDirectory/cache/html";
$wgUseGzip = false;
$wgUseETag = true;

# CSS-based preferences supposedly cause about 20 times slower page loads
# https://phabricator.wikimedia.org/rSVN63707
$wgAllowUserCssPrefs = false;

## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;


##
## Database settings
##

$wgDBtype = "XXX";
$wgDBserver = "XXX";
$wgDBname = "XXX";
$wgDBuser = "XXX";
$wgDBpassword = "XXX";

# MySQL specific settings
$wgDBprefix = "XXX";

# MySQL table options to use during installation or update
$wgDBTableOptions = "ENGINE=InnoDB, DEFAULT CHARSET=binary";


##
## Media settings
##

## To enable image uploads, make sure the 'images' directory
## is writable, then set this to true:
$wgEnableUploads = true;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";
$wgFileExtensions = array( 'png', 'gif', 'jpg', 'jpeg', 'svg' );
$wgSVGConverter = 'ImageMagick';
$wgMaxShellTime = 0;
$wgMaxShellMemory = 0;
$wgAllowTitlesInSVG = true;

# InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = false;

## If you use ImageMagick (or any other shell command) on a
## Linux server, this will need to be set to the name of an
## available UTF-8 locale
$wgShellLocale = "en_US.utf8";




##
## Skin settings
##

## Default skin: you can change the default skin. Use the internal symbolic
## names, ie 'vector', 'monobook':
$wgDefaultSkin = "vector";
$wgDefaultUserOptions['skin'] = 'vector';

# Enabled skins.
# The following skins were automatically enabled:
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Vector' );
$wgVectorResponsive = true;

# Load our custom extension that handles theme overrides
wfLoadExtension( 'PopPlanet' );

$wgPopHome = 'https://pop-planet.info/';
$wgPopNavBar = array(
    'Pop!_OS Homepage' => 'https://system76.com/pop',
    'Pop!_Docs' => 'https://pop.system76.com/docs/',
    'System76 Support' => 'https://support.system76.com/',
    'Realtime Chat' => 'https://chat.pop-os.org/',
    'Forum' => 'https://reddit.com/r/pop_os'
);

# Currently we don't use the navbar for internal links
#$wgPopNavBarSelectedDefault = 'Wiki';

$wgFooterIcons = ['copyright' => ['copyright' => '']];


##
## Access control settings
##

# disable anonymous editing
$wgEmailConfirmToEdit = true;
$wgDisableAnonTalk = true;
$wgGroupPermissions['*']['edit'] = false;

# extra rights for admins
$wgGroupPermissions['sysop']['deleterevision']  = true;

# disable uploads by normal users
$wgGroupPermissions['user']['upload']          = false;
$wgGroupPermissions['user']['reupload']        = false;
$wgGroupPermissions['user']['reupload-shared'] = false;
$wgGroupPermissions['autoconfirmed']['upload'] = false;

# maintainers' rights
$wgGroupPermissions['maintainer']['autopatrol'] = true;
$wgGroupPermissions['maintainer']['patrol'] = true;
$wgGroupPermissions['maintainer']['noratelimit'] = true;
$wgGroupPermissions['maintainer']['suppressredirect'] = true;
$wgGroupPermissions['maintainer']['rollback'] = true;
$wgGroupPermissions['maintainer']['browsearchive'] = true;
$wgGroupPermissions['maintainer']['apihighlimits'] = true;
$wgGroupPermissions['maintainer']['unwatchedpages'] = true;
$wgGroupPermissions['maintainer']['deletedhistory'] = true;
$wgGroupPermissions['maintainer']['deletedtext'] = true;

$wgEnableWriteAPI = true;
# disable user account creation via API
$wgAPIModules['createaccount'] = 'ApiDisabled';
# remove 'writeapi' right from users
$wgGroupPermissions['*']['writeapi'] = false;
$wgGroupPermissions['user']['writeapi'] = false;
# add 'writeapi' to autoconfirmed users, maintainers and admins
$wgGroupPermissions['autoconfirmed']['writeapi'] = true;
$wgGroupPermissions['maintainer']['writeapi'] = true;
$wgGroupPermissions['sysop']['writeapi'] = true;
# stricter conditions for 'autoconfirmed' promotion
$wgAutoConfirmAge = 86400*3; // three days
# require at least 20 normal edits before granting the 'writeapi' right
$wgAutoConfirmCount = 20;

# Enforce basic editing etiquette (FS#46190)
# We set the defaults for "minordefault" (disabled) and "forceeditsummary"
# (enabled) options and hide them from the user preferences dialog. Note that
# hiding the user preferences with $wgHiddenPrefs results in everybody using
# the defaults, regardless of the users' earlier preference.
$wgDefaultUserOptions["minordefault"] = 0;
$wgDefaultUserOptions["forceeditsummary"] = 1;
$wgHiddenPrefs[] = "minordefault";
$wgHiddenPrefs[] = "forceeditsummary";


##
## Additional extensions
##

# AbuseFilter extension
wfLoadExtension( 'AbuseFilter' );
$wgGroupPermissions['sysop']['abusefilter-modify'] = true;
$wgGroupPermissions['*']['abusefilter-log-detail'] = true;
$wgGroupPermissions['*']['abusefilter-view'] = true;
$wgGroupPermissions['*']['abusefilter-log'] = true;
$wgGroupPermissions['sysop']['abusefilter-private'] = true;
$wgGroupPermissions['sysop']['abusefilter-modify-restricted'] = true;
$wgGroupPermissions['sysop']['abusefilter-revert'] = true;

# filter groups
$wgAbuseFilterValidGroups = array( 'default', 'proofed' );
$wgAbuseFilterEmergencyDisableThreshold = array(
    'default' => 0.5,
    'proofed' => 1.0,
);
$wgAbuseFilterEmergencyDisableCount = array(
    'default' => 10,
    'proofed' => 65535,
);

# CheckUser extension
wfLoadExtension( 'CheckUser' );
$wgGroupPermissions['sysop']['checkuser'] = true;
$wgGroupPermissions['sysop']['checkuser-log'] = true;

# ParserFunctions extension
wfLoadExtension( 'ParserFunctions' );

# Interwiki extension
wfLoadExtension( 'Interwiki' );
$wgGroupPermissions['sysop']['interwiki'] = true;

# TitleKey extension
wfLoadExtension( 'TitleKey' );

# Nuke extension
wfLoadExtension( 'Nuke' );

# WikiEditor extension
wfLoadExtension( 'WikiEditor' );

# NoBogusUserpages extension
wfLoadExtension( 'NoBogusUserpages' );

# AntiSpoof extension
wfLoadExtension( 'AntiSpoof' );

# ContactPage extension
wfLoadExtension( 'ContactPage' );
$wgContactConfig['default'] = array(
	'RecipientUser' => 'Evertiro', // Must be the name of a valid account which also has a verified e-mail-address added to it.
	'SenderName' => 'Contact Form on ' . $wgSitename, // "Contact Form on" needs to be translated
	'SenderEmail' => null, // Defaults to $wgPasswordSender, may be changed as required
	'RequireDetails' => true, // Either "true" or "false" as required
	'IncludeIP' => false, // Either "true" or "false" as required
	'AdditionalFields' => array(
		'Text' => array(
			'label-message' => 'emailmessage',
			'type' => 'textarea',
			'rows' => 20,
			'required' => true,  // Either "true" or "false" as required
		),
	),
        // Added in MW 1.26
	'DisplayFormat' => 'table',  // See HTMLForm documentation for available values.
	'RLModules' => array(),  // Resource loader modules to add to the form display page.
	'RLStyleModules' => array(),  // Resource loader CSS modules to add to the form display page.
);

# Add contact page to footer
$wgHooks['SkinTemplateOutputPageBeforeExec'][] = function( $skin, &$template ) {
	$contactLink = Html::element( 'a', [ 'href' => $skin->msg( 'contactpage-url' )->escaped() ],
		$skin->msg( 'contactpage-label' )->text() );
	$template->set( 'contact', $contactLink );
	$template->data['footerlinks']['places'][] = 'contact';
	return true;
};

# Add donation link to footer
$wgHooks['SkinTemplateOutputPageBeforeExec'][] = function( $skin, &$template ) {
	$donateLink = Html::element( 'a', [ 'href' => 'https://paypal.me/evertiro' ],
		'Donate' );
	$template->set( 'donate', $donateLink );
	$template->data['footerlinks']['places'][] = 'donate';
	return true;
};

# Maintenance extension
wfLoadExtension( 'Maintenance' );
$wgGroupPermissions['sysop']['maintenance'] = true;

# RevisionSlider extension
wfLoadExtension( 'RevisionSlider' );

# ConfirmEdit
wfLoadExtension( 'ConfirmEdit' );
wfLoadExtension( 'ConfirmEdit/QuestyCaptcha' );
$wgCaptchaQuestions = [
	'What is the output of "apt|tail -1|xargs|base32"?' => 'KRUGS4ZAIFIFIIDIMFZSAU3VOBSXEICDN53SAUDPO5SXE4ZOBI======'
];
$wgCaptchaTriggers['edit'] = false; 
$wgCaptchaTriggers['create'] = false; 
$wgCaptchaTriggers['addurl'] = false;
$wgCaptchaTriggers['createaccount'] = true;
$wgCaptchaTriggers['badlogin'] = true;

# ReplaceText
wfLoadExtension( 'ReplaceText' );

# Stats
#wfLoadExtension( 'MatomoAnalytics' );
#$wgMatomoAnalyticsServerURL = 'https://stats.pop-planet.info';
#$wgMatomoAnalyticsTokenAuth = '79e418256b5b77141763b76e07792004';


##
## Debugging
##

//$wgShowExceptionDetails = true;
//$wgShowDBErrorBacktrace = true;
