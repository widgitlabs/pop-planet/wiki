<?php

namespace MediaWiki\Extensions\PopPlanet;

use MediaWiki\MediaWikiServices;

class Hooks
{
    public static function onBeforePageDisplay(\OutputPage &$out, \Skin &$skin)
    {
        $out->addModuleStyles('zzz.ext.popPlanet.styles');
        $out->addModules('zzz.ext.popPlanet.mobilemenu');
    }

    public static function onSkinTemplateOutputPageBeforeExec(\SkinTemplate $skinTemplate, \QuickTemplate $tpl)
    {
        $config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig('popplanet');
        $popNavBar = $config->get("PopNavBar");
        $popHome = $config->get("PopHome");
        $popNavBarSelected = $config->get("PopNavBarSelected");
        $popNavBarSelectedDefault = $config->get("PopNavBarSelectedDefault");

        ob_start();
        include __DIR__ . '/PopNavBar.php';
        $tpl->set('headelement', $tpl->get('headelement') . ob_get_clean());
    }
}