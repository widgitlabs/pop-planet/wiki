<?php

namespace MediaWiki\Extensions\PopPlanet;

/**
 * @var \SkinTemplate $skinTemplate
 * @var array $popNavBar
 * @var string $popHome
 * @var array $popNavBarSelected
 * @var string $popNavBarSelectedDefault
 */
?>
<div id="popnavbar" class="noprint">
    <div id="popnavbarlogo">
        <p><a id="logo" href="<?= $popHome ?>">Pop!_Planet</a></p>
    </div>
    <div id="popnavbarmenu">
        <ul id="popnavbarlist">
            <?php
            foreach ($popNavBar as $name => $url) {
                if (($skinTemplate->getTitle() == $name && in_array($name, $popNavBarSelected))
                    || (!(in_array($skinTemplate->getTitle(), $popNavBarSelected)) && $name == $popNavBarSelectedDefault)) {
                    $pnbClass = ' class="pnb-selected"';
                } else {
                    $pnbClass = '';
                }
                ?>
            <li id="pnb-<?= strtolower($name) ?>"<?= $pnbClass ?>><a href="<?= $url ?>" target="_blank"><?= $name ?></a></li><?php
            }
            ?>
        </ul>
    </div>
    <a id="popnavbarmenutoggle" href="#">
        <span></span>
    </a>
</div>